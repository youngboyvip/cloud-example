package vip.youngboy.gateway.converter;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.config.convert.NacosConfigConverter;
import org.springframework.cloud.gateway.route.RouteDefinition;

public class RouteDefinitionConverter implements NacosConfigConverter<RouteDefinition> {
    @Override
    public boolean canConvert(Class<RouteDefinition> targetType) {
        return RouteDefinition.class.equals(targetType);
    }

    @Override
    public RouteDefinition convert(String config) {
        return JSON.parseObject(config, RouteDefinition.class);
    }
}
