package com.alibaba.nacos.example.spring.cloud;

import com.alibaba.nacos.example.spring.cloud.provider.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author xiaojing
 */
@EnableFeignClients(basePackages = "com.alibaba.nacos.example.spring.cloud.provider")
@SpringBootApplication
@EnableDiscoveryClient
public class NacosConsumerApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosConsumerApplication.class, args);
    }

    @RestController
    public class TestController {

        private final RestTemplate restTemplate;

        private final Provider provider;

        @Autowired
        public TestController(RestTemplate restTemplatem, Provider provider) {
            this.restTemplate = restTemplatem;
            this.provider = provider;
        }

        @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
        public String echo(@PathVariable String str) {
            return restTemplate.getForObject("http://service-provider/echo/" + str, String.class);
        }

        @RequestMapping(value = "/feign/{str}", method = RequestMethod.GET)
        public String feign(@PathVariable String str) {
            return provider.echo(str);
        }

        @RequestMapping(value = "/hello", method = RequestMethod.GET)
        public String hello() {
            return provider.hello();
        }
    }

}
