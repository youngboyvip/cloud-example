package com.alibaba.nacos.example.spring.cloud.rest;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController implements IEchoController {

    @Override
    public String echo(String str) {
        return str;
    }

    @Override
    public String hello() {
        return "hello world";
    }
}
