package com.alibaba.nacos.example.spring.cloud.rest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface IEchoController {

    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    String echo(@PathVariable(name = "str") String str);

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    String hello();

}
